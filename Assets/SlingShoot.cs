﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlingShoot : MonoBehaviour
{
    //values for speed 
    //max power - jak starczy czasu
    //min power 
    public float speed;

    //public - lubie kontrolowac w inspektorze
    public Camera cam;
    public Camera cam2;
    public Vector3 startPoint;
    public Vector3 endPoint;

    public Vector2 force;
    public Vector2 minForce;
    public Vector2 maxForce;

    public Transform ballPrefab;

    private Rigidbody2D rigidbody;
    public Vector3 offset;


    public bool isClicked;
    private Transform ball;

    void Start()
    {
        //rigidbody = GetComponent<Rigidbody2D>();
        isClicked = false;
        cam.enabled = true;
        cam2.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isClicked)
        {
            //1 kliknięcie myszką
            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("Clicked");      //wyświetlenie dla pewności
                startPoint = cam.ScreenToWorldPoint(Input.mousePosition);
                startPoint.z = 10f;         //bez zmian na z
            }

            //gdzie dalej się znajduje kursor (rozciągnięcie wyrzutu)
            if (Input.GetMouseButton(0))
            {
                Vector3 livePoint = cam.ScreenToWorldPoint(Input.mousePosition);
                livePoint.z = 10f;      // j.w
            }

            //koniec 
            if (Input.GetMouseButtonUp(0))
            {
                endPoint = cam.ScreenToWorldPoint(Input.mousePosition);
                endPoint.z = 10f; //j.w


                //siła
                force = new Vector3(Mathf.Clamp(startPoint.x - endPoint.x, minForce.x, maxForce.x), Mathf.Clamp(startPoint.y - endPoint.y, minForce.y, maxForce.y),10f);

                //dodac siłę do rigidbody
                //rigidbody.AddForce(force * speed * Time.deltaTime);

                isClicked = false;


                ball = Instantiate(ballPrefab, transform.position, transform.rotation);
                ball.GetComponent<Rigidbody2D>().AddForce(force * speed * Time.deltaTime, ForceMode2D.Impulse);

                cam.enabled = false;
                cam2.enabled = true;

                cam2.GetComponent<CameraController>().target = ball;

                StartCoroutine(passive(1));

            }
           
        }
    }

    //spr czy obiekt został "kliknięty"
    private void OnMouseDown()
    {
        isClicked = true;


    }
    /*
    public void throwBall(Vector3 endPoint)
    {
        Rigidbody clone;
        clone = Instantiate(ball, endPoint, Quaternion.identity);


        clone.AddForce(force * speed * Time.deltaTime);
    }*/
    IEnumerator passive(int sec)
    {
        yield return new WaitForSeconds(sec);
        cam.enabled = true;
        cam2.enabled = false;
    }
}
